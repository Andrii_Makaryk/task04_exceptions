package com.makaryk;

import java.util.Arrays;

public class Group {
    private Student[] group;

    public Group() {

    }

    public Group(Student[] group) {
        this.group = group;
    }

    public Student[] getGroup() {
        return group;
    }

    public void setGroup(Student[] group) {
        this.group = group;
    }

    public Student getElement(int index){
        Student student=null;
        for(int i=0;i<group.length;i++){
            if(i==index){
                student=group[i];
            }
        }
        return student;
    }

    @Override
    public String toString() {
        return "Group{" +
                "group=" + Arrays.toString(group) +
                '}';
    }
}