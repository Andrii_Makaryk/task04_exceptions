package demo;

import com.makaryk.Group;
import com.makaryk.Student;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystemNotFoundException;

public class Main {
    private static String filepath = "D:\\exceptions\\src\\main\\resources.tx";

    public static void main(String[] args) {

        try {
            Student student = null;
            System.out.println(student.toString());


        } catch (NullPointerException ex) {
            System.out.println("NullPointerException: No object created!!!!");
        }
        BufferedReader reader = null;
        String line;

        try {
            reader = new BufferedReader(new FileReader(filepath));

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println("IOException found : FIlE NOT FOUND!!!!");
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {

            Student[] students = new Student[5];
            students[0] = new Student("Frank", "Math", 80.8);
            students[1] = new Student("Mark", "Math", 70.5);
            students[2] = new Student("Mett", "Math", 90.1);
            students[3] = new Student("David", "Math", 60.9);
            students[4] = new Student("Bill", "Math", 75.2);
            students[5] = new Student("Bill", "Math", 75.2);
            Group group = new Group(students);
            System.out.println(group.getElement(7));
            System.out.println(students[5]);
        } catch (IndexOutOfBoundsException ex3) {
            System.out.println("IndexOutOfBoundsException: Don't have that index.");
        }
    }
}

